﻿Public Class main
     
    Private Sub Panel1_Paint(sender As Object, e As PaintEventArgs) Handles Panel1.Paint
        e.Graphics.DrawRectangle(Pens.Black, 0, 0, DirectCast(sender, Panel).Width - 1, DirectCast(sender, Panel).Height - 1)
    End Sub

    Private Sub Panel2_Paint(sender As Object, e As PaintEventArgs) Handles Panel2.Paint
        e.Graphics.DrawRectangle(Pens.Black, 0, 0, DirectCast(sender, Panel).Width - 1, DirectCast(sender, Panel).Height - 1)
    End Sub

    Private Sub VelocityButton1_Click(sender As Object, e As EventArgs) Handles VelocityButton1.Click
        updateSize(TextBox2.Text, TextBox3.Text)
    End Sub

    Public Sub updateSize(w As Integer, h As Integer)
        Label7.Text = w * h * 3 & " characters"
        output.Size = New Size(w + 30, h + 53)
    End Sub

    Private Sub Panel3_Paint(sender As Object, e As PaintEventArgs) Handles Panel3.Paint
        e.Graphics.DrawRectangle(Pens.Black, 0, 0, DirectCast(sender, Panel).Width - 1, DirectCast(sender, Panel).Height - 1)
    End Sub

    Private Sub VelocityButton3_Click(sender As Object, e As EventArgs) Handles VelocityButton3.Click
        output.Show()
    End Sub

    Private Sub VelocityButton4_Click(sender As Object, e As EventArgs) Handles VelocityButton4.Click
        Dim nSfd As New SaveFileDialog
        If nSfd.ShowDialog = Windows.Forms.DialogResult.OK Then
            output.PictureBox1.Image.Save(nSfd.FileName, Imaging.ImageFormat.Png)
        End If
    End Sub

    Private Sub VelocityButton2_Click(sender As Object, e As EventArgs) Handles VelocityButton2.Click
        Dim inputValues As New List(Of Integer)
        Dim RGBAscii As New List(Of Color)
        For Each inputChar As String In TextBox1.Text
            inputValues.Add(Asc(inputChar))
        Next

        Do Until inputValues.Count Mod 3 = 0
            inputValues.Add(0)
        Loop

        For i = 0 To (inputValues.Count / 3) - 1
            RGBAscii.Add(Color.FromArgb(inputValues(i * 3).ToString, inputValues(i * 3 + 1).ToString, inputValues(i * 3 + 2).ToString))
        Next

        Dim outputBMP As New Bitmap(output.PictureBox1.Width, output.PictureBox1.Height)
        Dim g As Graphics = Graphics.FromImage(outputBMP)

        Dim x As Integer = 0
        Dim y As Integer = 0
        For Each col As Color In RGBAscii
            g.FillRectangle(New SolidBrush(col), New Rectangle(x, y, 1, 1))

            x += 1
            If x = output.PictureBox1.Width Then
                y += 1
                x = 0
            End If
        Next

        output.PictureBox1.Image = outputBMP
    End Sub 

    Private Sub VelocityButton7_Click(sender As Object, e As EventArgs) Handles VelocityButton7.Click
        Dim nOFD As New OpenFileDialog
        If nOFD.ShowDialog = Windows.Forms.DialogResult.OK Then
            input.PictureBox1.Image = Image.FromFile(nOFD.FileName)
        End If
        nOFD.Dispose()


        Label9.Visible = True


        Dim RGBAscii As New List(Of Color)
        Dim outValues As New List(Of Integer)

        Dim xPixelCount As Integer = 0
        Dim b As New Bitmap(Image.FromFile(nOFD.FileName))
        Dim x As Integer = 0
        Dim y As Integer = 0
        Dim exitPixel As Boolean = False
        Do Until exitPixel = True Or y = input.Height - 1
            Dim readPixel As Color = b.GetPixel(x, y)
            If (readPixel.R = 0) And (readPixel.G = 0) And (readPixel.B = 0) Then
                exitPixel = True
            Else
                RGBAscii.Add(b.GetPixel(x, y))
            End If
            x += 1
            If x = b.Width Then
                y += 1
                x = 0
            End If
            xPixelCount += 1
        Loop

        For Each col As Color In RGBAscii
            If (col.R = 0) And (col.G = 0) And (col.B = 0) Then
                Exit For
            Else
                TextBox4.AppendText(Chr(col.R))
                TextBox4.AppendText(Chr(col.G))
                TextBox4.AppendText(Chr(col.B))
            End If
            Label10.Text = "char count: " & TextBox4.Text.Length
            Application.DoEvents()
        Next

        MsgBox("Complete") 
    End Sub

    Private Sub VelocityButton6_Click(sender As Object, e As EventArgs) Handles VelocityButton6.Click
        input.Show()
    End Sub

    Private Sub Panel4_Paint(sender As Object, e As PaintEventArgs) Handles Panel4.Paint
        e.Graphics.DrawRectangle(Pens.Black, 0, 0, DirectCast(sender, Panel).Width - 1, DirectCast(sender, Panel).Height - 1)
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        Label11.Text = "char count: " & TextBox1.Text.Length
    End Sub
End Class
