# Imext

This version of Imext, is the finest yet. Imext converts text into an image.

How it works :
All inputted text is converted into a Ascii Value then inserted into a RGB colour sequence. The pixel is then drawn. Therefore 1 pixel can hold 3 characters effectively cutting down file size on larger text files.

Examples :
The Entire Romeo & Juliet play in an 256x256 image.

![Alt text](https://i.gyazo.com/255d45bcc32d52f61fc41c1e717297ce.png, "Romeo & Juliet")

Raw text size: 115kb

Converted Image size : 84.5kb





The book "The art of war" in an 156x153 image.

![Alt text](https://i.gyazo.com/008dbc9b654438756ad8a963931d50ab.png, "The art of war")

Raw text size: 61kb

Converted Image size : 34.2kb
