﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(main))
        Me.VelocityTabControl1 = New Imext_dv4.VelocityTabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.VelocityButton4 = New Imext_dv4.VelocityButton()
        Me.VelocityButton3 = New Imext_dv4.VelocityButton()
        Me.VelocityButton2 = New Imext_dv4.VelocityButton()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.VelocityButton1 = New Imext_dv4.VelocityButton()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.VelocityButton6 = New Imext_dv4.VelocityButton()
        Me.VelocityButton7 = New Imext_dv4.VelocityButton()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.RichTextBox2 = New System.Windows.Forms.RichTextBox()
        Me.VelocityTabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.SuspendLayout()
        '
        'VelocityTabControl1
        '
        Me.VelocityTabControl1.Alignment = System.Windows.Forms.TabAlignment.Left
        Me.VelocityTabControl1.Controls.Add(Me.TabPage1)
        Me.VelocityTabControl1.Controls.Add(Me.TabPage2)
        Me.VelocityTabControl1.Controls.Add(Me.TabPage3)
        Me.VelocityTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.VelocityTabControl1.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!)
        Me.VelocityTabControl1.ItemSize = New System.Drawing.Size(40, 130)
        Me.VelocityTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.VelocityTabControl1.Multiline = True
        Me.VelocityTabControl1.Name = "VelocityTabControl1"
        Me.VelocityTabControl1.SelectedIndex = 0
        Me.VelocityTabControl1.Size = New System.Drawing.Size(725, 207)
        Me.VelocityTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.VelocityTabControl1.TabIndex = 0
        Me.VelocityTabControl1.TextAlign = Imext_dv4.Helpers.TxtAlign.Center
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Label11)
        Me.TabPage1.Controls.Add(Me.VelocityButton4)
        Me.TabPage1.Controls.Add(Me.VelocityButton3)
        Me.TabPage1.Controls.Add(Me.VelocityButton2)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.Panel3)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.Panel2)
        Me.TabPage1.Controls.Add(Me.Panel1)
        Me.TabPage1.Location = New System.Drawing.Point(134, 4)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(587, 199)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Encrypt"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label11.Font = New System.Drawing.Font("Consolas", 8.25!)
        Me.Label11.Location = New System.Drawing.Point(104, 8)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(222, 13)
        Me.Label11.TabIndex = 16
        Me.Label11.Text = "char count:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'VelocityButton4
        '
        Me.VelocityButton4.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!)
        Me.VelocityButton4.ForeColor = System.Drawing.Color.White
        Me.VelocityButton4.Location = New System.Drawing.Point(104, 157)
        Me.VelocityButton4.Name = "VelocityButton4"
        Me.VelocityButton4.Size = New System.Drawing.Size(70, 35)
        Me.VelocityButton4.TabIndex = 9
        Me.VelocityButton4.Text = "Save"
        Me.VelocityButton4.TextAlign = Imext_dv4.Helpers.TxtAlign.Center
        '
        'VelocityButton3
        '
        Me.VelocityButton3.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!)
        Me.VelocityButton3.ForeColor = System.Drawing.Color.White
        Me.VelocityButton3.Location = New System.Drawing.Point(180, 157)
        Me.VelocityButton3.Name = "VelocityButton3"
        Me.VelocityButton3.Size = New System.Drawing.Size(70, 35)
        Me.VelocityButton3.TabIndex = 8
        Me.VelocityButton3.Text = "Output"
        Me.VelocityButton3.TextAlign = Imext_dv4.Helpers.TxtAlign.Center
        '
        'VelocityButton2
        '
        Me.VelocityButton2.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!)
        Me.VelocityButton2.ForeColor = System.Drawing.Color.White
        Me.VelocityButton2.Location = New System.Drawing.Point(256, 157)
        Me.VelocityButton2.Name = "VelocityButton2"
        Me.VelocityButton2.Size = New System.Drawing.Size(70, 35)
        Me.VelocityButton2.TabIndex = 7
        Me.VelocityButton2.Text = "Convert"
        Me.VelocityButton2.TextAlign = Imext_dv4.Helpers.TxtAlign.Center
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Consolas", 8.0!)
        Me.Label5.Location = New System.Drawing.Point(330, 102)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(31, 13)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Info"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Label6)
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Location = New System.Drawing.Point(333, 118)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(247, 74)
        Me.Panel3.TabIndex = 6
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(14, 15)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(161, 14)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "max storage capactiy :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(14, 44)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(91, 14)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "x characters"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Consolas", 8.0!)
        Me.Label2.Location = New System.Drawing.Point(6, 7)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(37, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Input"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Consolas", 8.0!)
        Me.Label1.Location = New System.Drawing.Point(330, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(31, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Size"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.VelocityButton1)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.TextBox2)
        Me.Panel2.Controls.Add(Me.TextBox3)
        Me.Panel2.Location = New System.Drawing.Point(333, 23)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(247, 74)
        Me.Panel2.TabIndex = 3
        '
        'VelocityButton1
        '
        Me.VelocityButton1.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!)
        Me.VelocityButton1.ForeColor = System.Drawing.Color.White
        Me.VelocityButton1.Location = New System.Drawing.Point(182, 17)
        Me.VelocityButton1.Name = "VelocityButton1"
        Me.VelocityButton1.Size = New System.Drawing.Size(54, 42)
        Me.VelocityButton1.TabIndex = 7
        Me.VelocityButton1.Text = "Set"
        Me.VelocityButton1.TextAlign = Imext_dv4.Helpers.TxtAlign.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(14, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 14)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "width:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(14, 44)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(56, 14)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "height:"
        '
        'TextBox2
        '
        Me.TextBox2.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(76, 12)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(100, 22)
        Me.TextBox2.TabIndex = 5
        Me.TextBox2.Text = "128"
        '
        'TextBox3
        '
        Me.TextBox3.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(76, 41)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(100, 22)
        Me.TextBox3.TabIndex = 6
        Me.TextBox3.Text = "128"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Location = New System.Drawing.Point(9, 23)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Padding = New System.Windows.Forms.Padding(1)
        Me.Panel1.Size = New System.Drawing.Size(317, 129)
        Me.Panel1.TabIndex = 1
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TextBox1.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(1, 1)
        Me.TextBox1.MaxLength = 2147483647
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(315, 127)
        Me.TextBox1.TabIndex = 0
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Label10)
        Me.TabPage2.Controls.Add(Me.Label9)
        Me.TabPage2.Controls.Add(Me.VelocityButton6)
        Me.TabPage2.Controls.Add(Me.VelocityButton7)
        Me.TabPage2.Controls.Add(Me.Label8)
        Me.TabPage2.Controls.Add(Me.Panel4)
        Me.TabPage2.Location = New System.Drawing.Point(134, 4)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(587, 199)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Decrypt"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label10.Font = New System.Drawing.Font("Consolas", 8.25!)
        Me.Label10.Location = New System.Drawing.Point(9, 157)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(73, 13)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "char count:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Consolas", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Red
        Me.Label9.Location = New System.Drawing.Point(8, 180)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(349, 13)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "*The application is not frozen, your image is converting."
        Me.Label9.Visible = False
        '
        'VelocityButton6
        '
        Me.VelocityButton6.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!)
        Me.VelocityButton6.ForeColor = System.Drawing.Color.White
        Me.VelocityButton6.Location = New System.Drawing.Point(433, 158)
        Me.VelocityButton6.Name = "VelocityButton6"
        Me.VelocityButton6.Size = New System.Drawing.Size(70, 35)
        Me.VelocityButton6.TabIndex = 13
        Me.VelocityButton6.Text = "Show"
        Me.VelocityButton6.TextAlign = Imext_dv4.Helpers.TxtAlign.Center
        '
        'VelocityButton7
        '
        Me.VelocityButton7.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!)
        Me.VelocityButton7.ForeColor = System.Drawing.Color.White
        Me.VelocityButton7.Location = New System.Drawing.Point(509, 158)
        Me.VelocityButton7.Name = "VelocityButton7"
        Me.VelocityButton7.Size = New System.Drawing.Size(70, 35)
        Me.VelocityButton7.TabIndex = 12
        Me.VelocityButton7.Text = "Convert"
        Me.VelocityButton7.TextAlign = Imext_dv4.Helpers.TxtAlign.Center
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Consolas", 8.0!)
        Me.Label8.Location = New System.Drawing.Point(8, 8)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(43, 13)
        Me.Label8.TabIndex = 11
        Me.Label8.Text = "Output"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.TextBox4)
        Me.Panel4.Location = New System.Drawing.Point(11, 24)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Padding = New System.Windows.Forms.Padding(1)
        Me.Panel4.Size = New System.Drawing.Size(568, 129)
        Me.Panel4.TabIndex = 10
        '
        'TextBox4
        '
        Me.TextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TextBox4.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox4.Location = New System.Drawing.Point(1, 1)
        Me.TextBox4.MaxLength = 2147483647
        Me.TextBox4.Multiline = True
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(566, 127)
        Me.TextBox4.TabIndex = 0
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.RichTextBox2)
        Me.TabPage3.Location = New System.Drawing.Point(134, 4)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(587, 199)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Info"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'RichTextBox2
        '
        Me.RichTextBox2.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox2.Location = New System.Drawing.Point(6, 8)
        Me.RichTextBox2.Name = "RichTextBox2"
        Me.RichTextBox2.Size = New System.Drawing.Size(573, 183)
        Me.RichTextBox2.TabIndex = 1
        Me.RichTextBox2.Text = resources.GetString("RichTextBox2.Text")
        '
        'main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(725, 207)
        Me.Controls.Add(Me.VelocityTabControl1)
        Me.Name = "main"
        Me.Text = "Imext - main form"
        Me.VelocityTabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents VelocityTabControl1 As Imext_dv4.VelocityTabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents VelocityButton1 As Imext_dv4.VelocityButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents VelocityButton4 As Imext_dv4.VelocityButton
    Friend WithEvents VelocityButton3 As Imext_dv4.VelocityButton
    Friend WithEvents VelocityButton2 As Imext_dv4.VelocityButton
    Friend WithEvents VelocityButton6 As Imext_dv4.VelocityButton
    Friend WithEvents VelocityButton7 As Imext_dv4.VelocityButton
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents RichTextBox2 As System.Windows.Forms.RichTextBox
End Class
